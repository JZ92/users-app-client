import styled from "styled-components/macro";
import { Link } from "react-router-dom";

export const Wrapper = styled.div`
  min-width: 36rem;
  background-color: white;
  box-shadow: 0 0 2rem 0 rgba(0, 0, 0, 0.2),
    0 0.5rem 0.5rem 0 rgba(0, 0, 0, 0.24);
  padding: 1rem;
  margin-top: 12rem;
  margin-bottom: 15rem;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  padding: 10px;
`;

export const Input = styled.input`
  margin: 1rem 0 1rem 0;
  padding: 1.5rem;
  font-size: 1.6rtem;
  border: 0;
  font-family: "Roboto", sans-serif;
  background: #f2f2f2;
`;

export const ListItem = styled.li`
  margin-bottom: 0.7em;
  font-size: 1.7rem;
  font-family: "Yanone Kaffeesatz", sans-serif;
`;

export const Button = styled.button`
  background: #4caf50;
  color: white;
  cursor: pointer;
  height: 3rem;

  :disabled {
    cursor: default;
  }
`;

export const StyledLink = styled(Link)`
  color: mintcream;
`;

export const Nav = styled.ul`
  display: flex;
  list-style-type: none;
  font-family: "Yanone Kaffeesatz";
  font-weight: 400;
  font-size: 2.8rem;
  justify-content: space-between;
  text-transform: uppercase;
`;

export const ListItemNav = styled.li`
  margin-right: 3rem;
  :last-child {
    margin-right: 0rem;
  }
`;
