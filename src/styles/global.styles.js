import { createGlobalStyle } from "styled-components/macro";

const GlobalStyles = createGlobalStyle`
@import url("https://fonts.googleapis.com/css?family=Roboto:400,700");
    html,
    body {
      min-height: 100%;
    }
    html {
      font-size: 10px;
    }
    body {
      background: #76b852;
      display: flex;
      min-height: 100vh;
      justify-content: center;
      align-items: center;  
      font-family: "Roboto", sans-serif;
      background: papayawhip;
      background: -moz-linear-gradient(
        top,
        #f5f5dc 0%,
        #d2b48c 100%
      ); /* FF3.6-15 */
      background: -webkit-linear-gradient(
        top,
        #f5f5dc 0%,
        #d2b48c 100%
      ); /* Chrome10-25,Safari5.1-6 */
      background: linear-gradient(
        to bottom,
        #f5f5dc 0%,
        #d2b48c 100%
      ); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    }
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
     
    }
    li {
      list-style-type: none;
    }
`;

export default GlobalStyles;
