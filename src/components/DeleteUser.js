import React, { useState, useRef } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Button, Wrapper, Form, Input } from "../styles/StyledComponents";

const DeleteUser = () => {
  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(null);
  const deleteRef = useRef(null);

  const history = useHistory();

  const onDelete = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);
      await axios.delete(
        `http://localhost:3030/api/users/${deleteRef.current.value}`
      );
      setLoading(false);
      if (failed) setFailed(null);
      deleteRef.current.value = "";
      history.push("/api/users");
      history.go(0);
    } catch (error) {
      console.log("Error! ", error);
      setLoading(false);
      setFailed(error.message);
    }
  };
  return (
    <Wrapper>
      <Form onSubmit={onDelete}>
        <Input
          type="text"
          placeholder="Enter id to delete"
          ref={deleteRef}
          name="delete"
        />
        <Button type="submit">Delete</Button>
      </Form>
      {failed && <h1>{failed}</h1>}
      {loading && <h1>Loading</h1>}
    </Wrapper>
  );
};

export default DeleteUser;
