import React, { useState, useEffect } from "react";
import axios from "axios";

import { Wrapper, ListItem } from "../styles/StyledComponents";

const GetAllUsers = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const url = `http://localhost:3030/api/users`;
        const { data } = await axios.get(url);

        setUsers(data);
        setLoading(false);
        if (failed) setFailed(null);
      } catch (error) {
        console.log("Error! ", error);
        setLoading(false);
        setFailed(error.message);
      }
    };
    fetchData();
  }, []);

  return (
    <Wrapper>
      {failed && <h1>{failed}</h1>}
      {loading && <h1>Loading</h1>}
      <ul>
        {users !== [] &&
          !loading &&
          users.map(({ _id, first_name, last_name, email }, i) => (
            <ListItem key={i}>
              <b>First name:</b> {first_name}
              &nbsp; <b>Last name:</b> {last_name}
              &nbsp; <b>Email:</b> {email}
              &nbsp; <b>Id:</b> {_id}
            </ListItem>
          ))}
      </ul>
    </Wrapper>
  );
};

export default GetAllUsers;
