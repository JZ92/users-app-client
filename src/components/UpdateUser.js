import React, { useState, useRef } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Button, Wrapper, Form, Input } from "../styles/StyledComponents";

const UpdateUser = () => {
  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(null);
  const refObj = useRef({});

  const history = useHistory();

  const onUpdate = async (event) => {
    event.preventDefault();
    const apiReqObj = {
      first_name: refObj.current["first_name"].value,
      last_name: refObj.current["last_name"].value,
      email: refObj.current["email"].value,
    };

    if (
      apiReqObj.first_name &&
      apiReqObj.last_name &&
      apiReqObj.email &&
      refObj.current["_id"]
    ) {
      try {
        setLoading(true);
        await axios.put(
          `http://localhost:3030/api/users/${refObj.current["_id"].value}`,
          apiReqObj
        );
        setLoading(false);
        emptyRefs();
        if (failed) setFailed(null);
        history.push("/api/users");
        history.go(0);
      } catch (error) {
        console.log("Error! ", error);
        setLoading(false);
        setFailed(error.message);
      }
    }
  };

  const emptyRefs = () => {
    refObj.current["first_name"].value = "";
    refObj.current["last_name"].value = "";
    refObj.current["email"].value = "";
    refObj.current["_id"].value = "";
  };

  return (
    <Wrapper>
      <Form onSubmit={onUpdate}>
        <Input
          type="text"
          placeholder="Enter the user's id to update"
          ref={(el) => (refObj.current["_id"] = el)}
          name="id"
        />
        <Input
          type="text"
          placeholder="First Name"
          ref={(el) => (refObj.current["first_name"] = el)}
          name="firstName"
        />
        <Input
          type="text"
          placeholder="Last Name"
          ref={(el) => (refObj.current["last_name"] = el)}
          name="lastName"
        />
        <Input
          type="text"
          placeholder="Email"
          ref={(el) => (refObj.current["email"] = el)}
          name="email"
        />
        <Button type="submit">Register</Button>
      </Form>
      {failed && <h1>{failed}</h1>}
      {loading && <h1>Loading</h1>}
    </Wrapper>
  );
};

export default UpdateUser;
