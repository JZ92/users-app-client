import React from "react";
import Links from "./Links";
import TopBar from "./TopBar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import GetAllUsers from "./GetAllUsers";
import DeleteUser from "./DeleteUser";
import AddUser from "./AddUser";
import GetUserById from "./GetUserById";
import UpdateUser from "./UpdateUser";

const App = () => {
  return (
    <Router>
      <TopBar>
        <Links />
      </TopBar>
      <Switch>
        <Route exact path="/api/users" component={GetAllUsers} />
        <Route path="/api/users/getbyid" component={GetUserById} />
        <Route path="/api/users/add" component={AddUser} />
        <Route path="/api/users/update" component={UpdateUser} />
        <Route path="/api/users/delete" component={DeleteUser} />
      </Switch>
    </Router>
  );
};

export default App;
