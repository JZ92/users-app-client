import React, { useState, useRef } from "react";
import axios from "axios";
import {
  Button,
  Wrapper,
  Form,
  Input,
  ListItem,
} from "../styles/StyledComponents";

const DeleteUser = () => {
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(null);
  const getByIdRef = useRef(null);

  const onGet = async (e) => {
    e.preventDefault();
    try {
      setLoading(true);
      const url = `http://localhost:3030/api/users/${getByIdRef.current.value}`;
      const { data } = await axios.get(url);
      setUser(data);

      getByIdRef.current.value = "";
      setLoading(false);
      if (failed) setFailed(null);
      //   if (failed) setFailed(false);
    } catch (error) {
      console.log("Error! ", error);
      setLoading(false);
      setFailed(error.message);
    }
  };

  const { first_name, last_name, email, _id } = user;
  return (
    <Wrapper>
      <Form onSubmit={onGet}>
        <Input
          type="text"
          placeholder="Enter id to search"
          ref={getByIdRef}
          name="getById"
        />
        <Button type="submit">Get</Button>
      </Form>
      {failed && <h1>{failed}</h1>}
      {loading ? (
        <h1>Loading data</h1>
      ) : (
        user?.first_name && (
          <ListItem>
            <b>First name:</b> {first_name}
            &nbsp; <b>Last name:</b> {last_name}
            &nbsp; <b>Email:</b> {email}
            &nbsp; <b>Id:</b> {_id}
          </ListItem>
        )
      )}
    </Wrapper>
  );
};

export default DeleteUser;
