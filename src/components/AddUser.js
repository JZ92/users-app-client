import React, { useState, useRef } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { Button, Wrapper, Form, Input } from "../styles/StyledComponents";

const AddUser = () => {
  const [loading, setLoading] = useState(false);
  const [failed, setFailed] = useState(null);
  const refObj = useRef({});

  const history = useHistory();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const apiReqObj = {
      first_name: refObj.current["first_name"].value,
      last_name: refObj.current["last_name"].value,
      email: refObj.current["email"].value,
    };

    if (apiReqObj.first_name && apiReqObj.last_name && apiReqObj.email) {
      try {
        setLoading(true);
        await axios.post("http://localhost:3030/api/users", apiReqObj);
        setLoading(false);
        if (failed) setFailed(null);
        emptyRefs();
        history.push("/api/users");
        history.go(0);
      } catch (error) {
        console.log("Error! ", error);
        setLoading(false);
        setFailed(error.message);
      }
    }
  };

  const emptyRefs = () => {
    refObj.current["first_name"].value = "";
    refObj.current["last_name"].value = "";
    refObj.current["email"].value = "";
  };

  return (
    <Wrapper>
      <Form onSubmit={handleSubmit}>
        <Input
          type="text"
          placeholder="First Name"
          ref={(el) => (refObj.current["first_name"] = el)}
          name="firstName"
        />
        <Input
          type="text"
          placeholder="Last Name"
          ref={(el) => (refObj.current["last_name"] = el)}
          name="lastName"
        />
        <Input
          type="text"
          placeholder="Email"
          ref={(el) => (refObj.current["email"] = el)}
          name="email"
        />

        <Button type="submit">Register</Button>
      </Form>
      {failed && <h1>{failed}</h1>}
      {loading && <h1>Loading</h1>}
    </Wrapper>
  );
};

export default AddUser;
