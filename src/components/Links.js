import React from "react";
import { Nav, ListItemNav, StyledLink } from "../styles/StyledComponents";

const Links = () => (
  <Nav>
    <ListItemNav>
      <StyledLink to="/api/users/">Show all users</StyledLink>
    </ListItemNav>
    <ListItemNav>
      <StyledLink to="/api/users/getbyid">Get user by id</StyledLink>
    </ListItemNav>
    <ListItemNav>
      <StyledLink to="/api/users/add">Add user</StyledLink>
    </ListItemNav>
    <ListItemNav>
      <StyledLink to="/api/users/update">Update user</StyledLink>
    </ListItemNav>
    <ListItemNav>
      <StyledLink to="/api/users/delete">Delete user</StyledLink>
    </ListItemNav>
  </Nav>
);

export default Links;
