import React from "react";
import styled from "styled-components/macro";

const TopBar = ({ children }) => (
  <Box>
    <Text>{children}</Text>
  </Box>
);

export default TopBar;

const Box = styled.div`
  background: slategray;
  width: 100%;
  height: 7rem;
  padding: 2rem;
  color: white;
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

const Text = styled.h1`
  font-size: 3rem;
  font-weight: normal;
  font-family: "Griffy", cursive;
  color: papayawhip;
  cursor: pointer;
  user-select: none;
  position: relative;
`;
